import * as express from 'express';
import Person from '../models/person';
import personCtrl from '../controllers/personController';


export default function setPersonRoutes(app){

const router= express.Router();
const person=new personCtrl();

//Person Routes
router.route('/persons').get(person.get);


}