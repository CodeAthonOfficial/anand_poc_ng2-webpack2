import * as express from 'express';
import * as mongoose from 'mongoose';
import setRoutes from './routes/personRoutes';
const app=new express();

//mongoose.connect("");
//const db=mongoose.connection;
app.set('port',3000);
setRoutes(app);
//db.on('error',console.error.bind(console,'connection error '));
//db.once('open',()=>{
//    setRoutes(app);
//});

app.get("/",(req,res)=>{

    res.write("hello POC");
    res.end();
});

app.listen(app.get('port'),()=>{

    console.log('POC is listening to port '+app.get('port'));
});