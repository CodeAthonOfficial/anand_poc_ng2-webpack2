import * as mongoose from 'mongoose';

const personSchema= mongoose.Schema({

    username:String,
    password:String
});

const Person=mongoose.model('Person',personSchema);

export default Person;