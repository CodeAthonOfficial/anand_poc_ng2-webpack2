var webpack=require("webpack");
var path=require("path");
var HtmlWebpackPlugin = require('html-webpack-plugin');

var config={
    entry:{
			polyfills:"./client/polyfills",
            vendor: "./client/vendor",
            main:    "./client/main"
    },
    devtool: "source-map",
    output:{
        path: path.resolve(__dirname, 'dist'),
        publicPath:"http://localhost:48491/",
        filename:'[name].bundle.js',
        sourceMapFilename: '[file].map'
    },
    resolve: {
    extensions: ['.js', '.ts']
  },
  module: {
    rules: [
      { test: /.ts$/, loader: 'awesome-typescript-loader' },
      { test: /\.html$/, loader: 'html-loader'    }
    ]
  },
  devServer: {
      contentBase: __dirname + '/dist/',
      port: '48491',
      host: 'localhost',
      historyApiFallback: true,
      watchOptions: {
        // if you're using Docker you may need this
        // aggregateTimeout: 300,
        // poll: 1000,
        ignored: /node_modules/
      }
  },
  
    plugins: [
        //other plugins if any
        new HtmlWebpackPlugin({
            template: './client/app/index.html', //your template file
            filename: 'index.html',
            inject: 'body'
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: ['polyfills', 'vendor', 'main']
        })
    ]
}


module.exports=config;